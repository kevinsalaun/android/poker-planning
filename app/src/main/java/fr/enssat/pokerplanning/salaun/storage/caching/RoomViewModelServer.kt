package fr.enssat.pokerplanning.salaun.storage.caching

import androidx.fragment.app.FragmentActivity
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.network.*
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.storage.SessionView
import fr.enssat.pokerplanning.salaun.storage.permanent.*
import fr.enssat.pokerplanning.salaun.utils.Generator
import fr.enssat.pokerplanning.salaun.utils.LogManager


class RoomViewModelServer(activity: FragmentActivity) : RoomViewModel(activity) {
    private val _unicastServer = ServerSocket(context, this::onReceiveMessage)
    val _anonymousVotes = arrayListOf<Vote>()

    override fun onCleared() {
        _unicastServer.stopListening()
        super.onCleared()
    }

    init {
        _username = PStorageManager.getUsername(context)
        _activeParticipantsSet.add(_username)
        //_activeParticipants.postValue(_activeParticipantsSet.toList())
        _activeParticipants.value = _activeParticipantsSet.toList()
        _channelCode.value = Generator.generateChannelCode(Constants.MIN_CHANNEL_CODE, Constants.MAX_CHANNEL_CODE)
        refreshHeader()
        _sessionView.value = SessionView.VOTE_DESCRIPTION
        _unicastServer.startListening()
        LogManager.print(this, "becomHost: ${_channelCode.value}")
    }


    /*** Check status ***/
    override fun isHost(): Boolean {
        return true
    }


    /*** Request management ***/
    override fun onReceiveMessage(rqt: String) {
        //LogManager.print(this, "onReceiveMessage - receives: $rqt")
        val request = Request.fromJson(rqt)
        val message = Message.fromJson(request.content)

        if (request.from != deviceIp) {
            if (request.channelCode == _channelCode.value) {
                if (_expectedAnswer.remove(request.reqId)) {
                    //expected response
                    LogManager.print(this, "onReceiveMessage - receives (expected response): ${request.content}")
                } else {
                    //expected request
                    LogManager.print(this, "onReceiveMessage - receives (expected request): ${request.content}")
                    when (message.type) {
                        "JoinSessionR" -> {
                            val messageCast = message as JoinSessionR
                            addInActiveParticipants(messageCast.participant)
                            val responseMessage = JoinSessionA(
                                    messageCast.participant,
                                    _username,
                                    deviceIp,
                                    if (_sessionView.value == SessionView.WAITING_VOTES) SessionView.VOTING_ROOM else _sessionView.value!!,
                                    activeParticipants.value!!,
                                    _voteDescription.value,
                                    if (_sessionView.value == SessionView.VOTE_COUNTING) votes.value!! else _anonymousVotes
                            )
                            showWarning("${messageCast.participant} ${R.string.player_join_room}")
                            val response = generateRequest(responseMessage, request.reqId)
                            sendOnMulticast(response)
                            LogManager.print(this, "REQUEST JoinSessionA SENDED")
                        }
                        "VotingForm" -> {
                            val messageCast = message as VotingForm
                            val anonymousVote = Vote(messageCast.vote.username, "")
                            if (addInVotes(messageCast.vote)) {
                                _anonymousVotes.add(anonymousVote)
                            }
                            val response = NotifyNewVote(anonymousVote)
                            val respRequest = generateRequest(response)
                            sendOnMulticast(respRequest)
                            LogManager.print(this, "REQUEST NotifyNewVote SENDED")
                        }
                        "NotifyParticipantLeave" -> {
                            val messageCast = message as NotifyParticipantLeave
                            if(removeInActiveParticipants(messageCast.participant)) {
                                val warning = context.resources.getString(R.string.player_left_room)
                                showWarning("${messageCast.participant} $warning")
                            }
                            LogManager.print(this, "REQUEST NotifyParticipantLeave SENDED")
                        }
                    }
                }
            } else {
                LogManager.print(this, "onReceiveMessage - the message was for another room: MESSAGE ${message.type}")
            }
        }
    }


    /*** actions ***/
    override fun leaveSession(notify: Boolean) {
        _unicastServer.stopListening()
        _anonymousVotes.clear()
        val warning = context.resources.getString(R.string.you_left_room)
        showWarning(warning)

        super.leaveSession(notify)
    }

    override fun sendIndividualVote(card: String) {
        val vote = Vote(_username, card)
        val anonymousVote = Vote(_username, "")
        removeInAvailableCard(card)
        if (addInVotes(vote)) {
            _anonymousVotes.add(anonymousVote)
        }

        val message = NotifyNewVote(anonymousVote)
        val request = generateRequest(message)
        sendOnMulticast(request)

        navigateTo(SessionView.WAITING_VOTES)
    }

    fun publishVoteDescription(voteDescription: VoteDescription) {
        _voteDescription.value = voteDescription
        loadDeck(voteDescription.deck)

        val message = NewVotingRound(voteDescription)
        val request = generateRequest(message)
        sendOnMulticast(request)

        navigateTo(SessionView.VOTING_ROOM)
    }

    fun publishVoteCounting() {
        val message = PublishVoteCounting(votes.value ?: emptyList())
        val request = generateRequest(message)
        sendOnMulticast(request)

        PStorageManager.historize(context, HistoryEntry(_voteDescription.value!!, VoteSession(votes.value ?: emptyList())))

        navigateTo(SessionView.VOTE_COUNTING)
    }

    fun restart() {
        redefineVotes(listOf())
        _anonymousVotes.clear()
        _voteDescription.value = null
        _expectedAnswer.clear()

        LogManager.print(this, "VOTE LIST MUST BE EMPTY $votes")
        LogManager.print(this, "ANONYMOUS VOTE LIST MUST BE EMPTY $_anonymousVotes")

        navigateTo(SessionView.VOTE_DESCRIPTION)
    }
}