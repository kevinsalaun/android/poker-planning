package fr.enssat.pokerplanning.salaun.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentWaitingVotesBinding
import fr.enssat.pokerplanning.salaun.recycler_view.RVAdapterAnonymousVote
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelServer
import fr.enssat.pokerplanning.salaun.utils.Appearance

class WaitingVotesFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentWaitingVotesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_waiting_votes, container, false)
        var model: RoomViewModel = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity())).get(RoomViewModel::class.java)
        model._header.observe(this, Observer {
            binding.headerText.text = model._header.value
        })

        Appearance.hide(binding.waitingHostText)
        val deactiveBtn = {
            Appearance.hide(binding.showResultBtn)
        }

        // prepares the recycler view
        val adapter = RVAdapterAnonymousVote()
        binding.listReceivedVote.adapter = adapter
        binding.listReceivedVote.layoutManager = LinearLayoutManager(context)
        if (model.votes.value != null) {
            adapter.votes = model.votes.value!!
        }
        model.votes.observe(this, Observer { votes ->
            adapter.votes = votes
            binding.receivedVoteListText.text = "${resources.getString(R.string.received_vote_list_text)} (${votes.size} votes)"
        })

        // prepares button listeners
        if(model.isHost()) {
            model = model as RoomViewModelServer
            binding.showResultBtn.setOnClickListener {
                model.publishVoteCounting()
            }
        } else {
            Appearance.show(binding.waitingHostText)
            deactiveBtn()
        }

        return binding.root
    }
}