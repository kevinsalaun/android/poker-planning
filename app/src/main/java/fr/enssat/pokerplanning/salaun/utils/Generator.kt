package fr.enssat.pokerplanning.salaun.utils

import java.security.SecureRandom

class Generator {
    companion object {
        fun generateChannelCode(min: Int=1, max: Int=999999): String {
            val random = SecureRandom()
            return (random.nextInt(max - min + 1) + min).toString().padStart(max.toString().length, '0')
        }
    }
}