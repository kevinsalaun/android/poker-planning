package fr.enssat.pokerplanning.salaun.storage.caching

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class RoomViewModelFactory(val context: FragmentActivity, val isHost:Boolean=false): ViewModelProvider.Factory {
    // The factory allows us to pass the context as an argument
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if(isHost) {
            RoomViewModelServer(context) as T
        } else {
            RoomViewModelClient(context) as T
        }
    }
}