package fr.enssat.pokerplanning.salaun.recycler_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.pockerplanning.R

class RVHolderCard(view: View): RecyclerView.ViewHolder(view) {
    private var cardView = view.findViewById<View>(R.id.card)
    private var cardTextTop = view.findViewById<TextView>(R.id.cardTopLeft)
    private var cardTextCenter = view.findViewById<TextView>(R.id.cardCenter)
    private var cardTextBottom = view.findViewById<TextView>(R.id.cardBottomRight)

    fun bind(card: String, callback: (m: String) -> Unit) {
        cardTextTop?.text = card
        cardTextCenter?.text = card
        cardTextBottom?.text = card
        cardView!!.setOnClickListener {
            callback(card)
        }
    }
}

class RVAdapterCard(private val cb: (String) -> Unit): RecyclerView.Adapter<RVHolderCard>()  {
    var cards: List<String> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVHolderCard {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_card, parent, false)
        return RVHolderCard(view)
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    override fun onBindViewHolder(holder: RVHolderCard, position: Int) {
        val card: String = cards[position]
        holder.bind(card, cb)
    }
}