package fr.enssat.pokerplanning.salaun.utils

import android.content.Context
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.storage.Constants

class Dialog {
    companion object {
        fun confirmDialog(context: Context, title: String, msg: String, cbConfirm: () -> Unit, cbCancel: () -> Unit) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(msg)

            builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                dialog.dismiss()
                cbConfirm()
            }

            builder.setNegativeButton(android.R.string.no) { dialog, _ ->
                dialog.dismiss()
                cbCancel()
            }

            builder.show()
        }

        fun loginDialog(context: Context, cbConfirm: (String) -> Unit) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Utilisateur")

            // include the layout
            val layoutInflater:LayoutInflater = LayoutInflater.from(context)
            val view = layoutInflater.inflate(R.layout.dialog_username, null)
            val usernameEditText = view.findViewById(R.id.username) as EditText
            builder.setView(view)

            // buttons
            builder.setPositiveButton("Valider") { dialog, _ ->
                val username = usernameEditText.text
                if (username.trim().length > 3) {
                    cbConfirm(username.trim().toString())
                    dialog.dismiss()
                } else {
                    usernameEditText.error = context.resources.getString(R.string.not_enough_characters).replace("#", Constants.MIN_CHARACTERS_USERNAME.toString(), false)
                }
            }
            builder.show()
        }
    }
}