package fr.enssat.pokerplanning.salaun

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.ActivityMainBinding
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory
import fr.enssat.pokerplanning.salaun.utils.Dialog


class MainActivity: AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        lateinit var mainNavController: NavController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNUSED_VARIABLE")
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        val navController = this.findNavController(R.id.myNavHostFragment)

        appBarConfiguration = AppBarConfiguration(navController.graph)
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)

        mainNavController = navController
    }

    override fun onSupportNavigateUp(): Boolean {
        val model = ViewModelProviders.of(this, RoomViewModelFactory(this)).get(RoomViewModel::class.java)

        var state = true
        val reloadActivity = {
            finish()
            overridePendingTransition(0, 0)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        if (model.inSession()) {
            val onConfirm = {
                // leave session then redirect to main page
                model.leaveSession()
                mainNavController.navigate(R.id.titleFragment)
                reloadActivity()
            }
            Dialog.confirmDialog(this, resources.getString(R.string.warning_title), resources.getString(R.string.confirm_quit_room), onConfirm, {})
        } else {
            state = mainNavController.navigateUp()
        }

        if (state && mainNavController.currentDestination?.id == R.id.titleFragment) {
            // reset view model
            reloadActivity()
        }
        return state
    }

    // binds android back button
    override fun onBackPressed() {
        if (mainNavController.currentDestination?.id == R.id.titleFragment) {
            super.onBackPressed()
        } else {
            onSupportNavigateUp()
        }
    }
}
