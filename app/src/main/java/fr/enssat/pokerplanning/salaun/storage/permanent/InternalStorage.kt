package fr.enssat.pokerplanning.salaun.storage.permanent

import android.content.Context
import android.content.SharedPreferences
import fr.enssat.pokerplanning.salaun.storage.Constants

class InternalStorage {
    companion object {
        fun set(context: Context, index: String, json: String) {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(Constants.PREFS_FILENAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(index, json).apply()
        }

        fun get(context: Context, index: String): String? {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(Constants.PREFS_FILENAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString(index, "")
        }

        fun del(context: Context, index: String) {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(Constants.PREFS_FILENAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(index, "").apply()
        }

        fun clear(context: Context) {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(Constants.PREFS_FILENAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.clear().apply()
        }
    }
}