package fr.enssat.pokerplanning.salaun.storage

import com.example.android.pockerplanning.R

class Constants {
    companion object {
        val PREFS_FILENAME = "fr.enssat.pokerplanning.salaun"

        val HISTORY_LENGTH = 5
        val HISTORY_KEY = "history"
        val USERNAME_KEY = "username"

        val MIN_CHANNEL_CODE = 1
        val MAX_CHANNEL_CODE = 999999

        val FIBONACCI_DECK: List<String> = listOf("0", "1/2", "1", "2", "3", "5", "8", "13", "20", "40", "100", "?", "∞")
        val IDEALDAYS_DECK: List<String> = listOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11+")

        val UNICAST_PORT = 3231
        val MULTICAST_PORT = 3232
        val BUFFER_SIZE = 4096
        val MULTICAST_ROOT = "228.1.2.3"
        val MAX_CLIENT_BACK_LOG = 20
        val JOIN_TIMEOUT = 3000.toLong()

        val MAX_CHARACTERS_FEATURE = 25
        val MAX_CHARACTERS_DESCRIPTION = 100
        val MIN_CHARACTERS_USERNAME = 3

        var PRINT_DEBUG = true
    }
}