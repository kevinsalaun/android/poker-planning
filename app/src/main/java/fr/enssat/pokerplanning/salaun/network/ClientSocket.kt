package fr.enssat.pokerplanning.salaun.network

import android.content.Context
import android.os.Handler
import android.os.Looper
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.utils.LogManager
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ClientSocket(val context: Context, val messageListener: (String) -> Unit, val connectionListener: (Boolean) -> Unit) {
    private lateinit var socket: Socket

    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }

    fun stop() {
        if (::socket.isInitialized && socket.isConnected) socket.close()
    }

    fun connect(serverIp:InetAddress, cb: (Boolean)->Unit) {
        Executors.newSingleThreadExecutor().execute {
            try {
                //val address = InetAddress.getByName(serverIp)
                //LogManager.print(this, "try connecting to $address:${Constants.UNICAST_PORT}")
                socket = Socket(serverIp, Constants.UNICAST_PORT)
                mainThread.execute { connectionListener(true) }
                cb(true)
            }
            catch(ex:Exception) {
                val msg = ex.message ?: "unable to connect server"
                mainThread.execute { messageListener(msg) }
                mainThread.execute { connectionListener(false) }
                cb(false)
            }
        }
    }

    fun sendAndReceive(msg: String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val data = msg.toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()

                val buffer = ByteArray(Constants.BUFFER_SIZE)
                val len = socket.getInputStream().read(buffer)
                val str = String(buffer, 0, len, StandardCharsets.UTF_8)

                //affiche le message reçu dans l'ui
                mainThread.execute { messageListener(str) }

            } catch (e: IOException) {
                val err = e.message ?: "unable to send to or receive from server"
                mainThread.execute { messageListener(err) }
                //mainThread.execute { connectionListener(false) }
                stop()
            }
        }
    }

    fun send(msg: String, remoteIp: InetAddress) {
        val cb = { status: Boolean ->
            if (status) {
                Executors.newSingleThreadExecutor().execute {
                    try {

                        val data = msg.toByteArray(StandardCharsets.UTF_8)
                        socket.getOutputStream().write(data)
                        socket.getOutputStream().flush()
                    } catch (e: IOException) {
                        val err = e.message ?: "unable to send to or receive from server"
                        mainThread.execute { messageListener(err) }
                        mainThread.execute { connectionListener(false) }
                        stop()
                    }
                }
            }
        }
        if (!::socket.isInitialized) {
            connect(remoteIp, cb)
        } else {
            cb(true)
        }
    }
}