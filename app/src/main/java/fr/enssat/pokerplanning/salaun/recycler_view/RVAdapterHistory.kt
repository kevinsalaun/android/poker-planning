package fr.enssat.pokerplanning.salaun.recycler_view

import android.content.Context
import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.storage.permanent.HistoryEntry
import fr.enssat.pokerplanning.salaun.utils.Appearance


class RVHolderHistory(view: View): RecyclerView.ViewHolder(view) {
    private var cardView = view.findViewById<CardView>(R.id.cardView)
    private var dateText = view.findViewById<TextView>(R.id.dateText)
    private var featureText = view.findViewById<TextView>(R.id.featureText)
    private var descriptionText = view.findViewById<TextView>(R.id.descriptionText)
    private var votesRV = view.findViewById<RecyclerView>(R.id.votesRV)
    private val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm")
    private var isVisible = false

    fun bind(historyEntry: HistoryEntry, context: Context) {
        val formattedDate = formatter.format(historyEntry.voteDescription.date)
        val feature = historyEntry.voteDescription.feature
        val description = historyEntry.voteDescription.description
        val deck = historyEntry.voteDescription.deck
        val voteList = historyEntry.voteSession.votes

        // defines the show/hide functions and hide details
        val hideDetails = {
            Appearance.hide(descriptionText)
            Appearance.hide(votesRV)
        }
        val showDetails = {
            Appearance.show(descriptionText)
            Appearance.show(votesRV)
        }
        hideDetails()

        // fills the text field
        featureText.text = "${context.resources.getString(R.string.feature_lbl_colon)} $feature ($deck)"
        dateText.text = "date: $formattedDate"
        descriptionText.text = "${context.resources.getString(R.string.feature_description_lbl_colon)} $description"

        // prepare the recycler view
        val adapter = RVAdapterVote()
        votesRV.adapter = adapter
        votesRV.layoutManager = LinearLayoutManager(context)
        adapter.votes = voteList

        // set a click event to fold / unrold the details
        cardView.setOnClickListener {
            if (isVisible) {
                isVisible = false
                hideDetails()
            } else {
                isVisible = true
                showDetails()
            }
        }
    }
}

class RVAdapterHistory(val context: Context): RecyclerView.Adapter<RVHolderHistory>()  {
    var history: List<HistoryEntry> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVHolderHistory {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_history, parent, false)
        return RVHolderHistory(view)
    }

    override fun getItemCount(): Int {
        return history.size
    }

    override fun onBindViewHolder(holder: RVHolderHistory, position: Int) {
        val historyEntry: HistoryEntry = history[position]
        holder.bind(historyEntry, context)
    }
}