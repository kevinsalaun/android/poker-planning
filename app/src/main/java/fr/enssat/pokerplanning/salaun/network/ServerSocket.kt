package fr.enssat.pokerplanning.salaun.network

import android.content.Context
import android.os.Handler
import android.os.Looper
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.utils.LogManager
import java.io.IOException
import java.lang.Exception
import java.net.ServerSocket
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ServerSocket(val context: Context, val listener: (String) -> Unit) {
    private lateinit var serverSocket: ServerSocket
    private val IPADDRESS = NetworkUtils.getIpAddress(context)
    private var loop = true

    //liste des clients connectés au serveur
    private val allClientsConnected = mutableListOf<Reply>()

    fun startListening() {
        serverSocket = ServerSocket(Constants.UNICAST_PORT, Constants.MAX_CLIENT_BACK_LOG, IPADDRESS)

        // écoute toutes les nouvelles demandes de connections clientes
        // et crée une socket locale au serveur, reply dédiée a ce nouveau client.
        Executors.newSingleThreadExecutor().execute {
            while (loop) {
                try {
                    val newSocket = serverSocket.accept()
                    allClientsConnected.add(Reply(newSocket, listener))
                } catch (e: Exception) {
                    LogManager.print(this, "error: ${e.message}")
                    loop = false
                }
            }
        }
    }

    fun stopListening() {
        loop = false
        allClientsConnected.forEach { it.stop() }
        serverSocket.close()
    }

    class Reply(val socket: Socket, val listener: (String) -> Unit) {
        var loop = true
        val address = socket.inetAddress.address.toString()

        private val mainThread = object : Executor {
            val handler = Handler(Looper.getMainLooper())
            override fun execute(command: Runnable) {
                handler.post(command)
            }
        }

        init {
            Executors.newSingleThreadExecutor().execute {
                try {
                    while (loop) {
                        val buffer = ByteArray(Constants.BUFFER_SIZE)
                        val len = socket.getInputStream().read(buffer)
                        val msg = String(buffer, 0, len, StandardCharsets.UTF_8)

                        //affiche le message reçu dans l'ui
                        mainThread.execute { listener(msg) }

                        //répond avec message + adresse
                        /*val data = Message.toJson(PongMessage(msg, socket.remoteSocketAddress.toString()))
                                .toByteArray(StandardCharsets.UTF_8)
                        socket.getOutputStream().write(data)
                        socket.getOutputStream().flush()*/
                    }
                } catch (e: IOException) {
                    LogManager.print(this, "Client $address down")
                }
            }
        }

        fun stop() {
            loop = false
            socket.close()
        }
    }
}