package fr.enssat.pokerplanning.salaun.controllers.additional

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentSettingsBinding
import fr.enssat.pokerplanning.salaun.storage.permanent.PStorageManager
import fr.enssat.pokerplanning.salaun.utils.Dialog

class SettingsFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)

        val onValid: (String) -> Unit = { username ->
            PStorageManager.setUsername(requireContext(), username)
        }

        binding.changeUsernameBtn.setOnClickListener {
            Dialog.loginDialog(requireContext(), onValid)
        }

        return binding.root
    }
}
