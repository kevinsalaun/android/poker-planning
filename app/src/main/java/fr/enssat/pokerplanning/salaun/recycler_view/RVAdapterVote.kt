package fr.enssat.pokerplanning.salaun.recycler_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.storage.permanent.Vote

class RVHolderVote(view: View): RecyclerView.ViewHolder(view) {
    private val usernameText = view.findViewById<TextView>(R.id.username)
    private var cardTextTop = view.findViewById<TextView>(R.id.cardTopLeft)
    private var cardTextCenter = view.findViewById<TextView>(R.id.cardCenter)
    private var cardTextBottom = view.findViewById<TextView>(R.id.cardBottomRight)

    fun bind(vote: Vote) {
        cardTextTop?.text = vote.opinion
        cardTextCenter?.text = vote.opinion
        cardTextBottom?.text = vote.opinion
        usernameText?.text = vote.username
    }
}

class RVAdapterVote: RecyclerView.Adapter<RVHolderVote>() {
    var votes: List<Vote> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVHolderVote {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_vote, parent, false)
        return RVHolderVote(view)
    }

    override fun getItemCount(): Int {
        return votes.size
    }

    override fun onBindViewHolder(holder: RVHolderVote, position: Int) {
        holder.bind(votes[position])
    }
}