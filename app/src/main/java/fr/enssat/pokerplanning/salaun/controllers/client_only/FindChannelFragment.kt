package fr.enssat.pokerplanning.salaun.controllers.client_only

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentFindChannelBinding
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelClient
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory

class FindChannelFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentFindChannelBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_find_channel, container, false)
        var model: RoomViewModel = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity())).get(RoomViewModel::class.java)

        model = model as RoomViewModelClient
        binding.nextBtn.setOnClickListener {
            if (binding.channelCodeText.text != null && binding.channelCodeText.text!!.isNotEmpty()) {
                val channelCode = binding.channelCodeText.text.toString()
                model.joinChannel(channelCode)
            } else {
                Toast.makeText(activity, R.string.please_no_empty, Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }
}
