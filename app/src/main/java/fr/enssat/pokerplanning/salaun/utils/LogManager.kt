package fr.enssat.pokerplanning.salaun.utils

import android.util.Log
import fr.enssat.pokerplanning.salaun.storage.Constants

class LogManager {
    companion object {
        /*fun print(tag: String, msg: String) {
            if (Constants.printDebug) {
                Log.d(tag, msg)
            }
        }*/

        fun print(obj: Any, msg: String) {
            if (Constants.PRINT_DEBUG) {
                Log.d("DEBUG - ${obj.javaClass.simpleName}", msg)
            }
        }
    }
}