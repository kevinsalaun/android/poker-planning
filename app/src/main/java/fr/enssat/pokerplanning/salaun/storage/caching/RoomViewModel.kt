package fr.enssat.pokerplanning.salaun.storage.caching

import android.content.Context
import android.os.Handler
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.network.*
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.storage.SessionView
import fr.enssat.pokerplanning.salaun.storage.permanent.Vote
import fr.enssat.pokerplanning.salaun.storage.permanent.VoteDescription
import fr.enssat.pokerplanning.salaun.utils.LogManager
import java.net.InetAddress
import java.util.*


abstract class RoomViewModel(val pactivity: FragmentActivity) : ViewModel() {
    protected val context: Context = pactivity.applicationContext
    private lateinit var _navController: NavController
    protected var deviceIp: InetAddress = NetworkUtils.getIpAddress(context)
    private val _multicast = MultiCastAgent(this::onReceiveMessage)
    val _expectedAnswer = mutableListOf<String>()
    //////
    val _header = MutableLiveData<String>()
    val _sessionView = MutableLiveData<SessionView>(SessionView.NONE)
    val _channelCode = MutableLiveData<String>()
    val _voteDescription = MutableLiveData<VoteDescription>()
    lateinit var _username: String

    protected val _availableCardSet = mutableSetOf<String>()
    protected val _availableCard = MutableLiveData<List<String>>()
    val availableCard: LiveData<List<String>> get() = _availableCard

    protected val _activeParticipantsSet = mutableSetOf<String>()
    protected val _activeParticipants = MutableLiveData<List<String>>()
    val activeParticipants: LiveData<List<String>> get() = _activeParticipants

    private val _votesSet = mutableSetOf<Vote>()
    private val _votes = MutableLiveData<List<Vote>>()
    val votes: LiveData<List<Vote>> get() = _votes


    init {
        MultiCastAgent.wifiLock(context)
        _multicast.startReceiveLoop()
    }

    override fun onCleared() {
        _multicast.stopReceiveLoop()
        MultiCastAgent.releaseWifiLock()
        super.onCleared()
    }

    /*** redondant operations ***/
    fun redefineVotes(votes: List<Vote>) {
        _votesSet.clear()
        _votesSet.addAll(votes)
        //_votes.postValue(_votesSet.toList())
        _votes.value = _votesSet.toList()
    }

    fun addInVotes(vote: Vote): Boolean {
        var alreadyExists = false
        _votesSet.forEach {
            if (it.username == vote.username) {
                alreadyExists = true
                return@forEach //break
            }
        }
        if (!alreadyExists) {
            if (_votesSet.add(vote)) {
                //_votes.postValue(_votesSet.toList())
                _votes.value = _votesSet.toList()
                return true
            }
        }
        return false
    }

    fun redefineActiveParticipants(participants: List<String>) {
        _activeParticipantsSet.clear()
        _activeParticipantsSet.addAll(participants)
        //_activeParticipants.postValue(_activeParticipantsSet.toList())
        _activeParticipants.value = _activeParticipantsSet.toList()
        refreshHeader()
    }

    fun removeInActiveParticipants(participant: String): Boolean {
        if (_activeParticipantsSet.remove(participant)) {
            //_activeParticipants.postValue(_activeParticipantsSet.toList())
            _activeParticipants.value = _activeParticipantsSet.toList()
            refreshHeader()
            return true
        }
        return false
    }

    fun addInActiveParticipants(participant: String): Boolean {
        var alreadyExists = false
        _activeParticipantsSet.forEach { //useful?
            if (it == participant) {
                alreadyExists = true
                return@forEach //break
            }
        }
        if (!alreadyExists) {
            if (_activeParticipantsSet.add(participant)) {
                //_activeParticipants.postValue(_activeParticipantsSet.toList())
                _activeParticipants.value = _activeParticipantsSet.toList()
                refreshHeader()
                return true
            }
        }
        return false
    }

    fun redefineAvailableCard(cards: List<String>) {
        _availableCardSet.clear()
        _availableCardSet.addAll(cards)
        //_availableCard.postValue(_availableCardSet.toList())
        _availableCard.value = _availableCardSet.toList()
    }

    fun removeInAvailableCard(card: String): Boolean {
        if (_availableCardSet.remove(card)) {
            //_availableCard.postValue(_availableCardSet.toList())
            _availableCard.value = _availableCardSet.toList()
            return true
        }
        return false
    }

    /*fun addInAvailableCard(card: String): Boolean {
        var alreadyExists = false
        _availableCardSet.forEach {
            if (it == card) {
                alreadyExists = true
                return@forEach //break
            }
        }
        if (!alreadyExists) {
            if (_availableCardSet.add(card)) {
                //_availableCard.postValue(_availableCardSet.toList())
                _availableCard.value =_availableCardSet.toList()
                return true
            }
        }
        return false
    }*/


    /*** Init ***/
    fun setNavController(navController: NavController) {
        _navController = navController
    }


    /*** Check status ***/
    fun refreshHeader() {
        _header.value = "${_channelCode.value} (${_activeParticipantsSet.size} participants)"
    }

    fun inSession(): Boolean {
        return if (_sessionView.value != null) (_sessionView.value!! != SessionView.NONE) else false
    }

    fun showWarning(msg: String, cb: (()->Unit)?=null) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        if (cb != null) {
            Handler().postDelayed({
                cb()
            }, Toast.LENGTH_SHORT.toLong())
        }
    }

    abstract fun isHost(): Boolean


    /*** Request management ***/
    abstract fun onReceiveMessage(rqt: String)

    protected fun generateRequest(msg: Message, reqId: String=UUID.randomUUID().toString()): Request {
        val myIp = NetworkUtils.getIpAddress(context)
        val content = Message.toJson(msg)
        return Request(reqId, myIp, _channelCode.value!!, content)
    }

    protected fun sendOnMulticast(request: Request) {
        val requestStringified = Request.toJson(request)
        LogManager.print(this, "sends on multicast: $requestStringified")
        _multicast.send(requestStringified)
    }


    /*** actions ***/
    protected fun loadDeck(deck: String) {
        val cards = when(deck) {
            "Fibonacci" -> Constants.FIBONACCI_DECK
            "Ideal days" -> Constants.IDEALDAYS_DECK
            else -> listOf()
        }
        redefineAvailableCard(cards)
    }

    protected fun navigateTo(sessionView: SessionView) {
        _sessionView.value = sessionView
        LogManager.print(this, "NAVIGATE TO $sessionView !!")
        when(sessionView) {
            SessionView.VOTE_COUNTING -> _navController.navigate(R.id.voteCountingFragment)
            SessionView.WAITING_VOTES -> _navController.navigate(R.id.waitingVotesFragment)
            SessionView.VOTING_ROOM -> _navController.navigate(R.id.votingRoomFragment)
            SessionView.WAITING_HOST -> _navController.navigate(R.id.waitingHostFragment)
            SessionView.VOTE_DESCRIPTION -> {
                if (isHost()) {
                    _navController.navigate(R.id.voteDescriptionFragment)
                } else {
                    _navController.navigate(R.id.waitingHostFragment)
                }
            }
            SessionView.NONE -> {
                // removes navigation history and modelView data before redirect
                _navController.navigate(R.id.titleFragment)
                pactivity.finish()
                pactivity.overridePendingTransition(0, 0)
                pactivity.startActivity(pactivity.intent)
                pactivity.overridePendingTransition(0, 0)
                //pactivity.recreate()
            }
        }
    }

    abstract fun sendIndividualVote(card: String)

    open fun leaveSession(notify: Boolean=true) {
        if (notify && ::_username.isInitialized) {
            LogManager.print(this, "SORRY, BUT I WANT TO QUIT")
            val message = NotifyParticipantLeave(_username)
            val request = generateRequest(message)
            sendOnMulticast(request)
        }
        LogManager.print(this, "I'M LEFT")

        val treatment = {
            _multicast.stopReceiveLoop()
            MultiCastAgent.releaseWifiLock()

            _expectedAnswer.clear()
            _header.value = null
            _sessionView.value = SessionView.NONE
            _channelCode.value = null
            _voteDescription.value = null
            redefineAvailableCard(listOf())
            _activeParticipantsSet.clear()
            //_activeParticipants.postValue(_activeParticipantsSet.toList())
            _activeParticipants.value = _activeParticipantsSet.toList()
            _votesSet.clear()
            //_votes.postValue(_votesSet.toList())
            _votes.value = _votesSet.toList()

            if (::_navController.isInitialized) {
                navigateTo(SessionView.NONE)
            }
        }

        Handler().postDelayed({
            treatment()
        }, 2000)
    }
}