package fr.enssat.pokerplanning.salaun.storage.caching

import android.os.Handler
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.network.*
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.storage.SessionView
import fr.enssat.pokerplanning.salaun.storage.permanent.PStorageManager
import fr.enssat.pokerplanning.salaun.storage.permanent.Vote
import fr.enssat.pokerplanning.salaun.utils.LogManager
import java.net.InetAddress


class RoomViewModelClient(activity: FragmentActivity) : RoomViewModel(activity) {
    private val _unicastClient = ClientSocket(context, this::onReceiveMessage, this::socketChangeStatus)
    val _hostAdr = MutableLiveData<InetAddress>()
    //////
    val _hostName = MutableLiveData<String>()

    override fun onCleared() {
        _unicastClient.stop()
        super.onCleared()
    }


    /*** Check status ***/
    override fun isHost(): Boolean {
        return false
    }


    /*** Request management ***/
    override fun onReceiveMessage(rqt: String) {
        //LogManager.print(this, "onReceiveMessage - receives: $rqt")
        val request = Request.fromJson(rqt)
        val message = Message.fromJson(request.content)

        if (request.from != deviceIp) {
            if (request.channelCode == _channelCode.value) {
                /*LogManager.print(this, "_expectedAnswer: $_expectedAnswer")
            _expectedAnswer.forEach({
                LogManager.print(this, "item: $it")
            })
            LogManager.print(this, "request.reqId: ${request.reqId}")*/
                if (_expectedAnswer.remove(request.reqId)) {
                    //expected response
                    LogManager.print(this, "onReceiveMessage - receives (expected response): MESSAGE ${message.type} -> ${request.content}")
                    when (message.type) {
                        "JoinSessionA" -> {
                            LogManager.print(this, "JOINSESSIONA !!")
                            val messageCast = message as JoinSessionA
                            _hostAdr.value = messageCast.hostAdr
                            _hostName.value = messageCast.hostName
                            redefineActiveParticipants(messageCast.activeParticipants)
                            _voteDescription.value = messageCast.voteDescription
                            redefineVotes(messageCast.anonymousReceivedVotes)
                            if (messageCast.voteDescription != null) {
                                loadDeck(messageCast.voteDescription.deck)
                            }
                            refreshHeader()
                            navigateTo(message.activeView)
                        }
                    }
                } else {
                    //expected request
                    LogManager.print(this, "onReceiveMessage - receives (expected request): MESSAGE ${message.type} -> ${request.content}")

                    when (message.type) {
                        "JoinSessionA" -> {
                            LogManager.print(this, "JOINSESSIONA NOT FOR ME !!")
                            val messageCast = message as JoinSessionA
                            addInActiveParticipants(messageCast.participant)
                            showWarning("${messageCast.participant} ${R.string.player_join_room}")
                        }
                        "NotifyNewVote" -> {
                            val messageCast = message as NotifyNewVote
                            addInVotes(messageCast.vote)
                            LogManager.print(this, "REQUEST NotifyNewVote RECEIVED")
                        }
                        "NotifyParticipantLeave" -> {
                            val messageCast = message as NotifyParticipantLeave
                            if (removeInActiveParticipants(messageCast.participant)) {
                                if (messageCast.participant == _hostName.value) {
                                    val warning = context.resources.getString(R.string.host_left_room)
                                    showWarning(warning) { leaveSession(false) }
                                } else {
                                    showWarning("${messageCast.participant} ${R.string.player_left_room}")
                                }
                            }
                            LogManager.print(this, "REQUEST NotifyParticipantLeave RECEIVED")
                        }
                        "PublishVoteCounting" -> {
                            val messageCast = message as PublishVoteCounting
                            redefineVotes(messageCast.receivedVotes)
                            navigateTo(SessionView.VOTE_COUNTING)
                            LogManager.print(this, "REQUEST PublishVoteCounting RECEIVED")
                        }
                        "NewVotingRound" -> {
                            val messageCast = message as NewVotingRound
                            _voteDescription.value = messageCast.voteDescription
                            redefineVotes(listOf())
                            _expectedAnswer.clear()
                            loadDeck(messageCast.voteDescription.deck)
                            navigateTo(SessionView.VOTING_ROOM)
                        }
                    }
                }
            } else {
                LogManager.print(this, "onReceiveMessage - the message was for another room: MESSAGE ${message.type}")
            }
        }
    }

    private fun socketChangeStatus(status: Boolean) {
        if (!status) {
            val cb = {
                leaveSession(false)
            }
            val warning = context.resources.getString(R.string.host_left_room)
            showWarning(warning, cb)
        }
    }

    private fun sendOnUnicast(request: Request) {
        val requestStringified = Request.toJson(request)
        LogManager.print(this, "sends on unicast: $requestStringified")
        _unicastClient.send(requestStringified, _hostAdr.value!!)
    }

    /*** actions ***/
    override fun leaveSession(notify: Boolean) {
        _unicastClient.stop()
        _hostAdr.value = null
        _hostName.value = null

        super.leaveSession(notify)
    }

    override fun sendIndividualVote(card: String) {
        val vote = Vote(_username, card)

        val message = VotingForm(vote)
        val request = generateRequest(message)
        sendOnUnicast(request)
        //sendOnMulticast(request)

        navigateTo(SessionView.WAITING_VOTES)
    }

    fun joinChannel(channelCode: String) {
        LogManager.print(this, "joinChannel $channelCode")
        _channelCode.value = channelCode
        _username = PStorageManager.getUsername(context)

        val request = generateRequest(JoinSessionR(_username))
        _expectedAnswer.add(request.reqId)
        sendOnMulticast(request)

        Handler().postDelayed({
            if (!inSession()) {
                _expectedAnswer.remove(request.reqId)
                val warning = context.resources.getString(R.string.host_unreachable)
                showWarning(warning)
            }
        }, Constants.JOIN_TIMEOUT)
    }
}