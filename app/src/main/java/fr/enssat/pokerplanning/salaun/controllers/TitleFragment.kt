package fr.enssat.pokerplanning.salaun.controllers

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentTitleBinding
import fr.enssat.pokerplanning.salaun.MainActivity
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory
import fr.enssat.pokerplanning.salaun.storage.permanent.PStorageManager
import fr.enssat.pokerplanning.salaun.utils.Dialog

class TitleFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentTitleBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_title, container, false)

        val deactiveBtn = {
            binding.createBtn.isEnabled = false
            binding.joinBtn.isEnabled = false
        }
        val activeBtn = {
            binding.createBtn.isEnabled = true
            binding.joinBtn.isEnabled = true
        }

        // prepares button listeners
        deactiveBtn()
        binding.createBtn.setOnClickListener {
            val model = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity(), true)).get(RoomViewModel::class.java)
            model.setNavController(MainActivity.mainNavController)
            view!!.findNavController().navigate(R.id.action_titleFragment_to_voteDescriptionFragment)
        }
        binding.joinBtn.setOnClickListener {
            val model = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity(), false)).get(RoomViewModel::class.java)
            model.setNavController(MainActivity.mainNavController)
            view!!.findNavController().navigate(R.id.action_titleFragment_to_findChannelFragment)
        }

        // checks if the username is already known
        if (PStorageManager.getUsername(requireContext()).isEmpty()) {
            val onValid: (String) -> Unit = {username ->
                PStorageManager.setUsername(requireContext(), username)
                activeBtn()
            }
            Dialog.loginDialog(requireContext(), onValid)
        } else {
            activeBtn()
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!, view!!.findNavController())
                ||super.onOptionsItemSelected(item)
    }
}
