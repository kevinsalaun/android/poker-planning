package fr.enssat.pokerplanning.salaun.controllers.host_only

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentVoteDescriptionBinding
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelServer
import fr.enssat.pokerplanning.salaun.storage.permanent.VoteDescription
import fr.enssat.pokerplanning.salaun.utils.Appearance
import kotlinx.android.synthetic.main.fragment_vote_description.*
import java.util.*

class VoteDescriptionFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentVoteDescriptionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_vote_description, container, false)
        var model: RoomViewModel = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity())).get(RoomViewModel::class.java)
        model._header.observe(this, Observer {
            binding.headerText.text = model._header.value
        })

        // prepares button listeners
        model = model as RoomViewModelServer
        binding.nextBtn.setOnClickListener {
            val featureEditText: EditText = binding.featureText
            val descriptionEditText: EditText = binding.descriptionText
            val feature = featureText.text!!.trim()
            val description = descriptionEditText.text!!.trim()
            var valid = true

            // check feature
            if (feature.isEmpty()) {
                featureEditText.error = getString(R.string.please_no_empty)
                valid = false
            } else if(feature.length > Constants.MAX_CHARACTERS_FEATURE) {
                featureEditText.error = "${getString(R.string.too_many_characters)} (max. ${Constants.MAX_CHARACTERS_FEATURE})"
                valid = false
            }

            // check description
            if(description.length > Constants.MAX_CHARACTERS_DESCRIPTION) {
                descriptionEditText.error = "${getString(R.string.too_many_characters)} (max. ${Constants.MAX_CHARACTERS_DESCRIPTION})"
                valid = false
            }

            if (valid) {
                val feature = feature.toString()
                val description = description.toString()
                val deckRadioButton = binding.deckRadioGroup.findViewById(binding.deckRadioGroup.checkedRadioButtonId) as RadioButton
                val deck = deckRadioButton.text.toString()
                val voteDescription = VoteDescription(deck, feature, description, Date())
                Appearance.hideKeyboard(requireActivity())
                model.publishVoteDescription(voteDescription)
            }
        }

        return binding.root
    }
}
