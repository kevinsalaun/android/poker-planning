package fr.enssat.pokerplanning.salaun.network

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Handler
import android.os.Looper
import fr.enssat.pokerplanning.salaun.storage.Constants
import fr.enssat.pokerplanning.salaun.utils.LogManager
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class MultiCastAgent(val listener: (String) -> Unit) {
    companion object {
        val MULTICAST_PORT = Constants.MULTICAST_PORT
        val MULTICAST_GROUP = InetAddress.getByName(Constants.MULTICAST_ROOT)
        val MAX_SIZE_MESSAGE = Constants.BUFFER_SIZE
        var multicastLock: WifiManager.MulticastLock? = null

        fun wifiLock(context: Context) {
            var lock = multicastLock
            if (lock == null) {
                val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
                lock = wifi.createMulticastLock("multicastLock")
                lock.setReferenceCounted(true)
                lock.acquire()
                multicastLock = lock
            }
        }

        fun releaseWifiLock() {
            multicastLock?.release()
            multicastLock = null
        }
    }

    private var loop = true
    private val socket = createSocket()

    private val mainThread = object: Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }

    private fun createSocket(): MulticastSocket {
        val socket = MulticastSocket(MULTICAST_PORT)
        socket.joinGroup(MULTICAST_GROUP)
        //LogManager.print(this, "create multicast socket:${MULTICAST_GROUP}/${MULTICAST_PORT}")
        return socket
    }

    fun startReceiveLoop() {
        Executors.newSingleThreadExecutor().execute {
            while (loop) {
                try {
                    val data = receive()
                    mainThread.execute{listener(data)}
                } catch (e: Exception) {
                    LogManager.print(this, "multicast error: $e")
                }
            }
        }
    }

    private fun receive(): String {
        val packet = DatagramPacket(ByteArray(MAX_SIZE_MESSAGE), MAX_SIZE_MESSAGE)
        //LogManager.print(this, "waiting for multicast datagram")
        socket.receive(packet)
        return String(packet.data, 0, packet.length, StandardCharsets.UTF_8)
    }

    fun stopReceiveLoop() {
        loop = false
        closeSocket()
    }

    private fun closeSocket() {
        //LogManager.print(this, "closing multicast socket")
        try {
            socket.leaveGroup(MULTICAST_GROUP)
            socket.close()
        } catch (e: Exception) {
            LogManager.print(this, "Socket was already closed")
        }
    }

    fun send(msg: String, sync: Boolean=false) {
        val treatment = {
            try {
                val data = msg.toByteArray(StandardCharsets.UTF_8)
                //LogManager.print(this, "publishing on multicast: ${String(data)}")
                val packet = DatagramPacket(data, 0, data.size, MULTICAST_GROUP, MULTICAST_PORT)
                socket.send(packet)
            } catch (e: Exception) {
                LogManager.print(this, "the socket was closed preventing the \"send\" operation")
            }
        }

        if (sync) {
            treatment()
        } else {
            Executors.newSingleThreadExecutor().execute {
                treatment()
            }
        }
    }
}