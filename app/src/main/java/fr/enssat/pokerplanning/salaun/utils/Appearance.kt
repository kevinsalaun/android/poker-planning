package fr.enssat.pokerplanning.salaun.utils

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager


class Appearance {
    companion object {
        fun show(element: View, focused: Boolean = false) {
            element.visibility = View.VISIBLE
            element.isEnabled = true
            if (focused) element.requestFocus()
        }

        fun hide(element: View) {
            element.visibility = View.GONE
            element.isEnabled = false
        }

        fun hideKeyboard(activity: Activity) {
            val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            // find the currently focused view, so we can grab the correct window token from it
            var view = activity.currentFocus
            // if no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
        }
    }
}