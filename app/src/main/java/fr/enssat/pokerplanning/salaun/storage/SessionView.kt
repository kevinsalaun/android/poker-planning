package fr.enssat.pokerplanning.salaun.storage

enum class SessionView {
    NONE,
    WAITING_HOST,
    WAITING_VOTES,
    VOTING_ROOM,
    VOTE_COUNTING,
    VOTE_DESCRIPTION
}