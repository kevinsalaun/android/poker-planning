package fr.enssat.pokerplanning.salaun.network

import com.google.gson.*
import fr.enssat.pokerplanning.salaun.storage.permanent.Vote
import java.lang.reflect.Type
import java.net.InetAddress

class Request(val reqId: String, val from: InetAddress, val channelCode: String, val content: String) {
    companion object {
        private val gson = Gson()
        fun toJson(src: Request) = gson.toJson(src)
        fun fromJson(str: String) = gson.fromJson<Request>(str, Request::class.java)
    }
}
