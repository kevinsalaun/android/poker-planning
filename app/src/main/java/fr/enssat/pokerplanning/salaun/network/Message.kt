package fr.enssat.pokerplanning.salaun.network

import com.google.gson.*
import fr.enssat.pokerplanning.salaun.storage.SessionView
import fr.enssat.pokerplanning.salaun.storage.permanent.Vote
import fr.enssat.pokerplanning.salaun.storage.permanent.VoteDescription
import java.lang.reflect.Type
import java.net.InetAddress

sealed class Message constructor(val type: String) {
    companion object {
        private val gson = GsonBuilder().registerTypeAdapter(Message::class.java, MessageSerializer()).create()
        fun toJson(src: Message) = gson.toJson(src)
        fun fromJson(str: String) = gson.fromJson<Message>(str, Message::class.java)
    }
}

class MessageSerializer: JsonDeserializer<Message> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Message {
        return when (json.asJsonObject["type"].asString) {
            "JoinSessionR" -> context.deserialize<JoinSessionR>(json, JoinSessionR::class.java)
            "JoinSessionA" -> context.deserialize<JoinSessionA>(json, JoinSessionA::class.java)
            "NewVotingRound" -> context.deserialize<NewVotingRound>(json, NewVotingRound::class.java)
            "VotingForm" -> context.deserialize<VotingForm>(json, VotingForm::class.java)
            "NotifyNewVote" -> context.deserialize<NotifyNewVote>(json, NotifyNewVote::class.java)
            "NotifyParticipantLeave" -> context.deserialize<NotifyParticipantLeave>(json, NotifyParticipantLeave::class.java)
            "PublishVoteCounting" -> context.deserialize<PublishVoteCounting>(json, PublishVoteCounting::class.java)

            "ErrorMessage" -> context.deserialize<ErrorMessage>(json, ErrorMessage::class.java)
            else -> UnknownMessage()
        }
    }
}

/******* Messages types ******/
class JoinSessionR(val participant: String): Message("JoinSessionR")
class JoinSessionA(val participant: String, val hostName: String, val hostAdr: InetAddress, val activeView: SessionView, val activeParticipants: List<String>, val voteDescription: VoteDescription?, val anonymousReceivedVotes: List<Vote>): Message("JoinSessionA")
class NewVotingRound(val voteDescription: VoteDescription): Message("NewVotingRound")
class VotingForm(val vote: Vote): Message("VotingForm")
class NotifyNewVote(val vote: Vote): Message("NotifyNewVote")
class NotifyParticipantLeave(val participant: String): Message("NotifyParticipantLeave")
class PublishVoteCounting(val receivedVotes: List<Vote>): Message("PublishVoteCounting")

class ErrorMessage(val msg: String): Message("ErrorMessage")
class UnknownMessage: Message("Unknown")
