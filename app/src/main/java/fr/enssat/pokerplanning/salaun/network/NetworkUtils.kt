package fr.enssat.pokerplanning.salaun.network

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import android.text.format.Formatter
import java.net.Inet4Address
import java.net.InetAddress

class NetworkUtils {
    companion object{
        fun getIpAddress(context: Context): InetAddress {
            var result : InetAddress?

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val linkInfos = cm.getLinkProperties(cm.activeNetwork)?.linkAddresses
                val address = linkInfos?.single { it -> it.address is Inet4Address }
                result = address?.address
            } else {
                val wm = context.getSystemService(WIFI_SERVICE) as WifiManager
                val ip = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
                result = InetAddress.getByName(ip)
            }

            return result ?: InetAddress.getByName("0.0.0.0")
        }
    }
}