package fr.enssat.pokerplanning.salaun.recycler_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.pockerplanning.R
import fr.enssat.pokerplanning.salaun.storage.permanent.Vote

class RVHolderAnonymousVote(view: View): RecyclerView.ViewHolder(view) {
    private var usernameText = view.findViewById<TextView>(R.id.username)

    fun bind(vote: Vote) {
        usernameText?.text = vote.username
    }
}

class RVAdapterAnonymousVote: RecyclerView.Adapter<RVHolderAnonymousVote>()  {
    var votes: List<Vote> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVHolderAnonymousVote {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_anonymous_vote, parent, false)
        return RVHolderAnonymousVote(view)
    }

    override fun getItemCount(): Int {
        return votes.size
    }

    override fun onBindViewHolder(holder: RVHolderAnonymousVote, position: Int) {
        val vote: Vote = votes[position]
        holder.bind(vote)
    }
}