package fr.enssat.pokerplanning.salaun.storage.permanent

import android.content.Context
import com.google.gson.Gson
import fr.enssat.pokerplanning.salaun.storage.Constants
import java.util.*
import kotlin.collections.ArrayList

class PStorageManager {
    companion object {
        private val gson = Gson()

        fun flushHistory(context: Context) {
            InternalStorage.del(context, Constants.HISTORY_KEY)
        }

        fun historize(context: Context, historyEntry: HistoryEntry) {
            var sessionHistory: SessionHistory
            var stringSessionHistory = InternalStorage.get(context, Constants.HISTORY_KEY)
            if (stringSessionHistory != null && stringSessionHistory.isNotEmpty()) {
                sessionHistory = gson.fromJson(stringSessionHistory, SessionHistory::class.java)
                if (sessionHistory.historyEntries.size == Constants.HISTORY_LENGTH) {
                    sessionHistory.historyEntries.removeAt(0)
                }
                sessionHistory.historyEntries.add(historyEntry)
            } else {
                sessionHistory = SessionHistory(arrayListOf(historyEntry))
            }
            InternalStorage.set(context, Constants.HISTORY_KEY, gson.toJson(sessionHistory))
        }

        fun getHistory(context: Context): SessionHistory? {
            var stringSessionHistory = InternalStorage.get(context, Constants.HISTORY_KEY)
            var sessionHistory: SessionHistory? = if (stringSessionHistory != null) gson.fromJson(stringSessionHistory, SessionHistory::class.java) else null
            return sessionHistory
        }

        fun setUsername(context: Context, username: String) {
            InternalStorage.set(context, Constants.USERNAME_KEY, username)
        }

        fun getUsername(context: Context): String {
            val username: String? = InternalStorage.get(context, Constants.USERNAME_KEY)
            return if (username.isNullOrEmpty()) "" else username
        }

        fun resetApp(context: Context) {
            InternalStorage.clear(context)
        }
    }
}


class SessionHistory(val historyEntries: ArrayList<HistoryEntry>)
class HistoryEntry(val voteDescription: VoteDescription, val voteSession: VoteSession)
class VoteDescription(val deck: String, val feature: String, val description: String, val date: Date)
class VoteSession(val votes: List<Vote>)
class Vote(val username: String, val opinion: String)
