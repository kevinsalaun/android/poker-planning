package fr.enssat.pokerplanning.salaun.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentVotingRoomBinding
import fr.enssat.pokerplanning.salaun.recycler_view.RVAdapterCard
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory

class VotingRoomFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentVotingRoomBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_voting_room, container, false)
        val model: RoomViewModel = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity())).get(RoomViewModel::class.java)
        model._header.observe(this, Observer {
            binding.headerText.text = model._header.value
        })
        binding.featureText.text = model._voteDescription.value?.feature
        binding.descriptionText.text = model._voteDescription.value?.description

        // prepares the recycler view
        val cb = { card: String ->
            model.sendIndividualVote(card)
        }

        // prepares the recycler view
        val adapter = RVAdapterCard(cb)
        binding.listVotes.adapter = adapter
        binding.listVotes.layoutManager = GridLayoutManager(context, 3)
        if (model.availableCard.value != null) {
            adapter.cards = model.availableCard.value!!
        }
        model.availableCard.observe(this, Observer { cards ->
            adapter.cards = cards
        })

        return binding.root
    }
}