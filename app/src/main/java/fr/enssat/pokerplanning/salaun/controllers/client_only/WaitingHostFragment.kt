package fr.enssat.pokerplanning.salaun.controllers.client_only

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentWaitingHostBinding
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModel
import fr.enssat.pokerplanning.salaun.storage.caching.RoomViewModelFactory

class WaitingHostFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentWaitingHostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_waiting_host, container, false)
        val model: RoomViewModel = ViewModelProviders.of(requireActivity(), RoomViewModelFactory(requireActivity())).get(RoomViewModel::class.java)
        model._header.observe(this, Observer {
            binding.headerText.text = model._header.value
        })

        return binding.root
    }
}
