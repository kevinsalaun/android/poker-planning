package fr.enssat.pokerplanning.salaun.controllers.additional

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.pockerplanning.R
import com.example.android.pockerplanning.databinding.FragmentHistoryBinding
import fr.enssat.pokerplanning.salaun.recycler_view.RVAdapterHistory
import fr.enssat.pokerplanning.salaun.storage.permanent.PStorageManager
import fr.enssat.pokerplanning.salaun.utils.LogManager

class HistoryFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false)

        val adapter = RVAdapterHistory(requireContext())
        binding.listHistory.adapter = adapter
        binding.listHistory.layoutManager = LinearLayoutManager(context)
        adapter.history = PStorageManager.getHistory(requireActivity())?.historyEntries ?: ArrayList()

        binding.resetHistoryBtn.setOnClickListener {
            PStorageManager.flushHistory(requireContext())
            adapter.history = ArrayList()
            Toast.makeText(context, R.string.history_cleared, Toast.LENGTH_SHORT).show()
        }

        return binding.root
    }
}
